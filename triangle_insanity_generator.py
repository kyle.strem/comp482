# Program for generating colors
import csv
import math

def generate_pi10_triangles():
    with open('project_triangles_pi10.csv', 'w') as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=',')

        color_hist = [0] * 30
        color_hist.insert(0, None)
        color_row = []
        rows = 0
        for i in range(1,1000001):
            color = 1 + (math.floor(i * (math.pi**10)) % 30)
            if color_hist[color] < 3:
                color_row.append(color)
                color_hist[color] += 1
            if len(color_row) == 3:
                csv_writer.writerow(color_row)
                rows += 1
                color_row = []
                if rows == 30:
                    print(i, color_hist)
                    return

def generate_e10_triangles():
    with open('project_triangles_e10.csv', 'w') as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=',')

        color_hist = [0] * 30
        color_hist.insert(0, None)
        color_row = []
        rows = 0
        for i in range(1,301):
            color = 1 + (math.floor(i * (math.exp(10))) % 30)
            if color_hist[color] < 3:
                color_row.append(color)
                color_hist[color] += 1
            if len(color_row) == 3:
                csv_writer.writerow(color_row)
                rows += 1
                color_row = []
                if rows == 30:
                    print(i, color_hist)
                    return

def generate_pi11_triangles():
    with open('project_triangles_pi11.csv', 'w') as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=',')

        color_hist = [0] * 30
        color_hist.insert(0, None)
        color_row = []
        rows = 0
        for i in range(1,1000001):
            color = 1 + (math.floor(i * (math.pi**11)) % 30)
            if color_hist[color] < 3:
                color_row.append(color)
                color_hist[color] += 1
            if len(color_row) == 3:
                csv_writer.writerow(color_row)
                rows += 1
                color_row = []
                if rows == 30:
                    print(i, color_hist)
                    return

def generate_e11_triangles():
    with open('project_triangles_e11.csv', 'w') as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=',')

        color_hist = [0] * 30
        color_hist.insert(0, None)
        color_row = []
        rows = 0
        for i in range(1,301):
            color = 1 + (math.floor(i * (math.exp(11))) % 30)
            if color_hist[color] < 3:
                color_row.append(color)
                color_hist[color] += 1
            if len(color_row) == 3:
                csv_writer.writerow(color_row)
                rows += 1
                color_row = []
                if rows == 30:
                    print(i, color_hist)
                    return

generate_pi10_triangles()
generate_e10_triangles()
generate_pi11_triangles()
generate_e11_triangles()
