# Algorithm for CLRS Heapsort
# Also, counts number of comparisons made and the sequence of them
import itertools

comparisons = itertools.count()

def swap(A, i, j):
    temp = A[i]
    A[i] = A[j]
    A[j] = temp

def max_heapify(A, i, heap_size, verbose=False, building=False):
    l = i * 2
    r = l + 1

    if l <= heap_size:
        if building and verbose: print("Building... Left  Child\tComparison {}({}) to {}({}): comp - {}".format(A[l], l, A[i], i, next(comparisons)+1))
        if verbose and not building: print("Left  Child\tComparison {}({}) to {}({}): comp - {}".format(A[l], l, A[i], i, next(comparisons)+1))
        if not verbose: next(comparisons)
        if A[l] > A[i]:
            largest = l
        else:
            largest = i
    else:
        largest = i

    if r <= heap_size:
        if building and verbose: print("Building... Right Child\tComparison {}({}) to {}({}): comp - {}".format(A[r], r, A[largest], largest, next(comparisons)+1))
        if verbose and not building: print("Right Child\tComparison {}({}) to {}({}): comp - {}".format(A[r], r, A[largest], largest, next(comparisons)+1))
        if not verbose: next(comparisons)
        if A[r] > A[largest]:
            largest = r

    if largest != i:
        swap(A, i, largest)
        # print(A[1:])
        max_heapify(A, largest, heap_size, verbose, building)

def build_max_heap(A, heap_size, verbose=False):
    if verbose: print(A)
    for i in range(heap_size//2, 0, -1):
        max_heapify(A, i, heap_size, verbose, True)

def heap_sort(A, heap_size, verbose=False):
    build_max_heap(A, heap_size, verbose)
    if verbose: print(A)
    for i in range(len(A[1:]), 1, -1):
        swap(A, 1, i)
        heap_size = heap_size - 1
        max_heapify(A, 1, heap_size, verbose)

def generate_permutations(A):
    return list(itertools.permutations(A[1:]))

def check_all_permutations(A):
    global comparisons
    perms = generate_permutations(A)

    max_comparisons = 0
    max_lst = []

    comp_repeats = 0

    for i in range(len(perms)):
        comparisons = itertools.count()
        A = list([None] + list(perms[i]))
        B = list([None] + list(perms[i]))

        # print(A)

        heap_size = len(A[1:])
        heap_sort(A, heap_size)

        comps = next(comparisons)

        if comps == max_comparisons:
            comp_repeats += 1
            # print("{}, {}".format(comps, B))

        if comps > max_comparisons:
            max_comparisons = comps
            max_lst = B
            comp_repeats = 0
            print("{}, {}".format(comps, B))


    print("---------\n{}, {}".format(max_comparisons, max_lst))
    print(comp_repeats)
    comparisons = itertools.count()
    heap_sort(max_lst,len(max_lst[1:]), True)


def main():
    A = [None,1,2,3,4,5,6,7,8,9]

    check_all_permutations(A)
    # A = [None,9,8,7,4,5,6,3,2,1]
    # heap_size = len(A[1:])
    #
    # heap_sort(A, heap_size)
    # print(comparisons)
    # print(A[1:])
    # build_max_heap(A, len(A[1:]))
    # print(comparisons)
    # print(A[1:])

if __name__ == "__main__":
    main()
