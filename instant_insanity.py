from itertools import combinations

adj_list =[[],[],[],[],[],[],[],[],[],[],[]]

lookup = {
    'A': 1,
    'B': 2,
    'C': 3,
    'D': 4,
    'E': 5,
    'F': 6,
    'G': 7,
    'H': 8,
    'I': 9,
    'K': 10
}

reverse_lookup = [None,'A','B','C','D','E','F','G','H','I','K']

def add_edge(cube, a, b):
    global lookup

    adj_list[cube].append((cube, lookup[a], lookup[b]))
    return

# Cube 1
add_edge(1,'D','E')
add_edge(1,'B','D')
add_edge(1,'I','I')

# Cube 2
add_edge(2,'E','H')
add_edge(2,'G','G')
add_edge(2,'B','E')

# Cube 3
add_edge(3,'F','K')
add_edge(3,'D','E')
add_edge(3,'H','I')

# Cube 4
add_edge(4,'A','B')
add_edge(4,'A','A')
add_edge(4,'I','K')

# Cube 5
add_edge(5,'G','K')
add_edge(5,'C','C')
add_edge(5,'A','D')

# Cube 6
add_edge(6,'D','E')
add_edge(6,'H','I')
add_edge(6,'F','G')

# Cube 7
add_edge(7,'C','F')
add_edge(7,'G','I')
add_edge(7,'B','E')

# Cube 8
add_edge(8,'F','G')
add_edge(8,'C','C')
add_edge(8,'B','C')

# Cube 9
add_edge(9,'A','B')
add_edge(9,'F','H')
add_edge(9,'F','F')

# Cube 10
add_edge(10,'H','K')
add_edge(10,'H','K')
add_edge(10,'A','D')

for row in adj_list:
    print(row)

histogram = [None] + [0] * 10

for row in adj_list:
    for opp in row:
        histogram[opp[1]] += 1
        histogram[opp[2]] += 1

print(histogram)


def generate_permutations(adj_list):
    perms = [[]]
    for cube_num in range(1, len(adj_list)):
        level_lst = []
        for pair in adj_list[cube_num]:
            for p in perms:
                level_lst.append(p + [pair])
        perms = level_lst

    return perms

perms = generate_permutations(adj_list)


def solve_half_solutions(perms):
    correct_paths = []
    for path in perms:
        colors_used = [None,0,0,0,0,0,0,0,0,0,0]
        for pair in path:
            if colors_used[pair[1]] >= 2 or colors_used[pair[2]] >= 2:
                break
            elif pair[1] == pair[2] and colors_used[pair[1]] >= 1:
                break
            colors_used[pair[1]] += 1
            colors_used[pair[2]] += 1
        if sum(colors_used[1:]) == 2 * len(path):
            correct_paths.append(path)

    return correct_paths

correct_paths = solve_half_solutions(perms)

def print_half_paths(correct_paths, verbose=False):
    print("Half: {}".format(len(correct_paths)))
    if verbose:
        for path in correct_paths:
            print(path)

print_half_paths(correct_paths, True)

def solve_complete_solutions(correct_paths):
    correct_full_paths = []
    for path in correct_paths:
        for path2 in correct_paths:
            conflict = False
            for pair in path:
                if pair in path2:
                    conflict = True
                    break
            if conflict == False:
                correct_full_paths.append([path, path2])
    return correct_full_paths

correct_full_paths = solve_complete_solutions(correct_paths)

def check_complete_solutions(correct_full_paths):
    successful_full_paths = []
    for path_pair in correct_full_paths:
        full_path = path_pair[0] + path_pair[1]
        colors_used = [None,0,0,0,0,0,0,0,0,0,0]
        for pair in full_path:
            if colors_used[pair[1]] >= 4 or colors_used[pair[2]] >= 4:
                break
            elif pair[1] == pair[2] and colors_used[pair[1]] >= 3:
                break
            colors_used[pair[1]] += 1
            colors_used[pair[2]] += 1
        if sum(colors_used[1:]) == 2 * len(full_path):
            successful_full_paths.append([path_pair[0], path_pair[1]])

    return successful_full_paths

def print_full_paths(correct_full_paths, verbose=False):
    global reverse_lookup

    simped_full_paths = set(frozenset(frozenset(p) for p in pp) for pp in correct_full_paths)

    print("Full: {}".format(len(simped_full_paths)))
    if verbose:
        for path_pair in simped_full_paths:
            print("-------------\n-------------")
            for path in path_pair:
                for row in sorted(list(path)):
                    r = list(row)
                    r[1] = reverse_lookup[row[1]]
                    r[2] = reverse_lookup[row[2]]
                    print(r)
                print("............")

print_full_paths(correct_full_paths, True)

# adj_list_list = list(combinations(adj_list[1:], 11))
# for i in range(len(adj_list_list)):
#     adj_list_list[i] = list(adj_list_list[i])
#     adj_list_list[i].insert(0,[])
# print(len(adj_list_list))
#
# for comb in adj_list_list:
#     print("Removed cube {}:".format(10 - adj_list_list.index(comb)))
#     perms = generate_permutations(comb)
#     half_solutions = solve_half_solutions(perms)
#     print_half_paths(half_solutions)
#     full_solutions = solve_complete_solutions(half_solutions)
#     full_solutions = check_complete_solutions(full_solutions)
#     print_full_paths(full_solutions)
#     print(full_solutions[0])
