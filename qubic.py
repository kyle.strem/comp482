# Group got 7 winning moves

# row = [None,None,None,None]
# slice = [row.copy(),row.copy(),row.copy(),row.copy()]
# board = [slice.copy(),slice.copy(),slice.copy(),slice.copy()]

board = [[[None for col in range(4)] for row in range(4)] for slice in range(4)]

winning_combs = []

corners = [
    (0,0,0),
    (0,0,3),
    (0,3,0),
    (0,3,3),
    (3,0,0),
    (3,0,3),
    (3,3,0),
    (3,3,3)
]

top_edges = [
    (0,0,1),
    (0,0,2),
    (0,1,0),
    (0,1,3),
    (0,2,0),
    (0,2,3),
    (0,3,1),
    (0,3,2)
]

bot_edges = [
    (3,0,1),
    (3,0,2),
    (3,1,0),
    (3,1,3),
    (3,2,0),
    (3,2,3),
    (3,3,1),
    (3,3,2)
]

left_edges = [
    (1,0,0),
    (1,3,0),
    (2,0,0),
    (2,3,0)
]

right_edges = [
    (1,0,3),
    (1,3,3),
    (2,0,3),
    (2,3,3)
]

top_fill = [
    (0,1,1),
    (0,1,2),
    (0,2,1),
    (0,2,2)
]

bot_fill = [
    (3,1,1),
    (3,1,2),
    (3,2,1),
    (3,2,2)
]

left_fill = [
    (1,1,0),
    (1,2,0),
    (2,1,0),
    (2,2,0)
]

right_fill = [
    (1,1,3),
    (1,2,3),
    (2,1,3),
    (2,2,3)
]

front_fill = [
    (1,3,1),
    (1,3,2),
    (2,3,1),
    (2,3,2)
]

back_fill = [
    (1,0,1),
    (1,0,2),
    (2,0,1),
    (2,0,2)
]

middle = [
    (1,1,1),
    (1,1,2),
    (1,2,1),
    (1,2,2),
    (2,1,1),
    (2,1,2),
    (2,2,1),
    (2,2,2)
]

all_but_middle = corners + \
                 top_edges + \
                 bot_edges + \
                 left_edges + \
                 right_edges + \
                 top_fill + \
                 bot_fill + \
                 left_fill + \
                 right_fill + \
                 front_fill + \
                 back_fill

# for coord in corners:
#     z,y,x = coord
#     board[z][y][x] = "C"
#
# for coord in top_edges + bot_edges + left_edges + right_edges:
#     z,y,x = coord
#     board[z][y][x] = "E"
#
# for coord in top_fill + bot_fill + left_fill + right_fill + front_fill + back_fill:
#     z,y,x = coord
#     board[z][y][x] = "F"
#
# for coord in middle:
#     z,y,x = coord
#     board[z][y][x] = "M"

def add_tuples(a, b):
    if len(a) != len(b):
        sys.exit("Tuples must be the same length to be added")
        return
    return tuple(map(lambda x, y: x + y, a, b))

directions = {
    "above":    (-1,0,0),
    "below":    (1,0,0),
    "up":       (0,-1,0),
    "down":     (0,1,0),
    "left":     (0,0,-1),
    "right":    (0,0,1)
}
directions["above-up"]=     add_tuples(directions["above"],directions["up"])
directions["above-down"]=   add_tuples(directions["above"],directions["down"])
directions["above-left"]=   add_tuples(directions["above"],directions["left"])
directions["above-right"]=  add_tuples(directions["above"],directions["right"])
directions["below-up"]=     add_tuples(directions["below"],directions["up"])
directions["below-down"]=   add_tuples(directions["below"],directions["down"])
directions["below-left"]=   add_tuples(directions["below"],directions["left"])
directions["below-right"]=  add_tuples(directions["below"],directions["right"])
directions["up-left"]=      add_tuples(directions["up"],directions["left"])
directions["up-right"]=     add_tuples(directions["up"],directions["right"])
directions["down-left"]=    add_tuples(directions["down"],directions["left"])
directions["down-right"]=   add_tuples(directions["down"],directions["right"])
directions["above-up-left"]=    add_tuples(directions["above-up"],directions["left"])
directions["above-up-right"]=   add_tuples(directions["above-up"],directions["right"])
directions["above-down-left"]=  add_tuples(directions["above-down"],directions["left"])
directions["above-down-right"]= add_tuples(directions["above-down"],directions["right"])
directions["below-up-left"]=    add_tuples(directions["below-up"],directions["left"])
directions["below-up-right"]=   add_tuples(directions["below-up"],directions["right"])
directions["below-down-left"]=  add_tuples(directions["below-down"],directions["left"])
directions["below-down-right"]= add_tuples(directions["below-down"],directions["right"])

for coord in all_but_middle:
    for dir in directions.keys():
        path = [coord]
        curr = coord
        out = False
        while not out:
            next = add_tuples(curr,directions[dir])
            z,y,x = next
            if z < 0 or z > 3 or y < 0 or y > 3 or x < 0 or x > 3:
                out = True
            else:
                path.append(next)
                curr = next

        path = sorted(path)
        if len(path) == 4:
            if path not in winning_combs:
                winning_combs.append(path)

for comb in winning_combs:
    print(comb)
print(len(winning_combs))
