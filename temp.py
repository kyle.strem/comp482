def recurse_rotation(sub, rotations):
    if not sub:
        yield rotations
    else:
        for i in range(3):
            rotations[sub[0]] = i
            yield from recurse_rotation(sub[1:], rotations)


def generate_rotations(subset):
    rotations = { tri: 0 for tri in subset }
    return recurse_rotation(subset, rotations)

rotation = generate_rotations([1,2,3,4])
print(next(rotation))
print(next(rotation))

# print(len([print(n) for n in generate_rotations([1,2,3,4])]))
