def parenthesized(exprs):
    if len(exprs) == 1:
        yield exprs[0]
    else:
        first_exprs = []
        last_exprs = list(exprs)
        while 1 < len(last_exprs):
            first_exprs.append(last_exprs.pop(0))
            for x in parenthesized(first_exprs):
                if 1 < len(first_exprs):
                    x = "({})".format(x)
                for y in parenthesized(last_exprs):
                    if 1 < len(last_exprs):
                        y = "({})".format(y)
                    yield "{}*{}".format(x,y)

from midterm_3_prob_5 import Matrix

A = Matrix(5,20)
B = Matrix(20,7)
C = Matrix(7,1)
D = Matrix(1,6)
E = Matrix(6,5)
F = Matrix(5,7)

# total = A.mult(B.mult(C).mult(D.mult(E.mult(F))))
#
# print(total.opts)

def mult_matrices(X,Y):
    return X.mult(Y)

def process_chain(chain_str):
    symbols = 'ABCDEF'
    global A,B,C,D,E,F
    lookup = {'A': A,
              'B': B,
              'C': C,
              'D': D,
              'E': E,
              'F': F}
    ops = []
    values = []

    i = 0
    while i < len(chain_str):
        if chain_str[i] == '(':
            ops.append(chain_str[i])

        elif chain_str[i] in symbols:
            val = lookup[chain_str[i]]
            values.append(val)

        elif chain_str[i] == ')':
            while len(ops) != 0 and ops[-1] != '(':
                val2 = values.pop()
                val1 = values.pop()
                op = ops.pop()

                values.append(mult_matrices(val1,val2))

            ops.pop()

        elif chain_str[i] == '*':
            # while len(ops) != 0:
            #     val2 = values.pop()
            #     val1 = values.pop()
            #     op = ops.pop()
            #
            #     values.append(mult_matrices(val1,val2))
            ops.append(chain_str[i])

        i += 1

    while len(ops) != 0:
        val2 = values.pop()
        val1 = values.pop()
        op = ops.pop()

        values.append(mult_matrices(val1, val2))

    return values[-1]

lst = parenthesized(list('ABCDEF'))
scores = []
pairs = {}
row = 1
for x in lst:
    score = process_chain(x).opts
    scores.append((score,x))
    pairs[score] = x
    row+=1

[print(n) for n in sorted(scores)]

# total = process_chain('A*(B*(C*((D*E)*F)))')
#
# print(total.opts)
