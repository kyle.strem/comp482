import csv
from itertools import combinations

def rotate(lst, n):
    return lst[n:] + lst[:n]

def create_color_matrix(file_name):
    with open(file_name, 'r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')

        mtx = []
        for row in csv_reader:
            mtx.append([int(c) for c in row])

        return mtx


color_mtx = create_color_matrix('project_triangles_e10.csv')

def generate_color_histogram(color_matrix):
    colors = [0] * 30
    colors.insert(0, None)

    for row in color_matrix:
        for color in row:
            colors[color] += 1

    return colors

color_hist = generate_color_histogram(color_mtx)

def check_color_histogram(hist):
    for n in color_hist[1:]:
        if n != 3:
            return False
    return True

def is_shift(color_rowA, color_rowB):
    perms = [color_rowA, rotate(color_rowA,1), rotate(color_rowA, -1)]
    if color_rowB in perms: return True
    else: return False

def generate_permutations(color_matrix):
    perms = [[]]
    for tri in color_matrix:
        print(len(perms))
        lst = []
        for p in perms:
            lst.append(p + tri)
            lst.append(p + rotate(tri, 1))
            lst.append(p + rotate(tri, -1))
        perms = lst
    return perms

def generate_combinations(mtx, r):
    lst = list(combinations(mtx, r))
    for i in range(len(lst)):
        lst[i] = list(lst[i])

    print(len(lst))
    return lst

def has_four(comb_list):
    for comb in comb_list:
        color_set = {}
        for row in comb:
            for color in row:
                if color in color_set:
                    if color_set[color] >= 3:
                        print(row, comb)
                        return True
                    else:
                        color_set[color] += 1
                else:
                    color_set[color] = 1
        # print(color_set, comb)
    return False

def solve(color_matrix):
    for i in range(len(color_matrix)-1):
        for j in range(i+1, len(color_matrix)):
            if set(color_matrix[i]) == set(color_matrix[j]):
                if not is_shift(color_matrix[i], color_matrix[j]):
                    return False

    done = False
    while not done:
        setA = set()
        setB = set()
        setC = set()
        for i in range(len(color_matrix)):
            if color_matrix[i][0] in setA or color_matrix[i][1] in setB or color_matrix[i][2] in setC:
                color_matrix[i] = rotate(color_matrix[i], 1)
                print(i, 'shift', color_matrix[i])
                break
            setA.add(color_matrix[i][0])
            setB.add(color_matrix[i][1])
            setC.add(color_matrix[i][2])
        if len(setA) == 30 and len(setB) == 30 and len(setC) == 30:
            done = True
    i = 1
    for row in color_matrix:
        print(i,row)
        i+=1
    return

print(solve(color_mtx))




def find_min_obstacle(color_matrix):
    i = 1
    while not has_four(generate_combinations(color_matrix, i)):
        i += 1

    return i





def check_puzzle(color_mtx, color_hist):
    if not check_color_histogram(color_hist):
        min_obstacle = find_min_obstacle(color_mtx)
        return min_obstacle
    else:
        return solve(color_mtx)

# print(check_puzzle(color_mtx, color_hist))
