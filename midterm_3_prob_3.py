import math

comp = 1

def sort(A,i,j):
    global comp
    # print(comp, "Comparing A[{}]({}) > A[{}]({}).".format(i, A[i], j, A[j]))
    comp += 1
    if A[i] > A[j]:
        print("Swapping {} - {}".format(A[i], A[j]))
        A[i], A[j] = A[j], A[i]
    if i+1 >= j:
        return
    k = math.floor((j-i+1)/3)
    sort(A, i, j-k)
    sort(A, i+k, j)
    sort(A, i, j-k)

A = [None,5,4,7,8,3,2,1]

sort(A,1,7)

print(A)
