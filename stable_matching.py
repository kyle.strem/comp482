import sys
# Male and female arrays for status (free/taken) and pair preferences

M = []
W = []

M_pref = []
W_pref = []

M_propose = []

def process_input():
    M_count = 0
    W_count = 0

    with open('match_pref.txt', 'r') as match_file:
        match_lines = match_file.readlines()

        for line in match_lines:
            if line[0] == "M":
                M_count += 1
                M.append("F")
                M_pref.append(list(map(lambda s: int(s), line[1:].split())))
                M_propose.append(-1)
            if line[0] == "W":
                W_count += 1
                W.append("F")
                W_pref.append(list(map(lambda s: int(s), line[1:].split())))

    if M_count != W_count:
        sys.exit("Must have equal number of men and women")

    print("Men Pref list:")
    print(M_pref)
    print("Women Pref list:")
    print(W_pref)


def is_free_man():
    out = False
    for m in M:
        if m == "F":
            out = True
            break;
    return out

def is_free_woman():
    for w in W:
        if w == "F":
            return True
    return False

def stable_matching():
    print(is_free_man())
    while is_free_man():
        # find a free man
        m = -1
        for i in range(len(M)):
            if M[i] == "F":
                m = i
                break

        # fine a woman that m has not proposed to yet
        M_propose[m] += 1
        if M_propose[m] >= len(W):
            continue
        w = M_propose[m]

        if W[w] == "F":
            M[m] = w
            W[w] = m
        else:
            bethrothed = W[w]
            m_pref_index = W_pref[w].index(m+1)
            bethrothed_pref_index = W_pref[w].index(bethrothed+1)

            if m_pref_index < bethrothed_pref_index:
                M[bethrothed] = "F"
                M[m] = w
                W[w] = m

    print(M)
    print(W)
    return

def process_output():
    for i in range(len(M)):
        outline = "M{} --- W{}".format(i+1, W[M[i]]+1)
        print(outline)
    return

process_input()
stable_matching()
process_output()
