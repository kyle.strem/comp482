import csv
from itertools import combinations

def rotate(lst, n):
    return lst[n:] + lst[:n]

def create_color_matrix(file_name):
    with open(file_name, 'r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')

        mtx = []
        for row in csv_reader:
            mtx.append([int(c) for c in row])

        return mtx

color_matrix = create_color_matrix('project_triangles_e11.csv')

def create_color_set_and_table(color_matrix):
    color_set = [set([i]) for i in range(1,len(color_matrix)+1)]
    color_table = [list() for i in range(1,len(color_matrix)+1)]
    color_table.insert(0,None)

    for i in range(len(color_matrix)):
        color_A = color_matrix[i][0]
        color_B = color_matrix[i][1]
        color_C = color_matrix[i][2]

        color_set[color_A-1].add(color_B)
        color_set[color_A-1].add(color_C)
        color_set[color_B-1].add(color_A)
        color_set[color_B-1].add(color_C)
        color_set[color_C-1].add(color_B)
        color_set[color_C-1].add(color_A)

        for color in color_matrix[i]:
            color_table[color].append(i+1)

    for i in range(len(color_set)):
        for j in range(len(color_set)):
            if not color_set[i].isdisjoint(color_set[j]):
                color_set[i] = color_set[i].union(color_set[j])
                color_set[j] = color_set[i].union(color_set[j])

    color_set = set(frozenset(i) for i in color_set)

    return color_set, color_table

color_set, color_table = create_color_set_and_table(color_matrix)


def create_triangle_subsets(color_set, color_table):
    triangle_subsets = [set(r) for r in color_table[1:]]

    for i in range(len(triangle_subsets)):
        for j in range(len(triangle_subsets)):
            if not triangle_subsets[i].isdisjoint(triangle_subsets[j]):
                triangle_subsets[i] = triangle_subsets[i].union(triangle_subsets[j])
                triangle_subsets[j] = triangle_subsets[j].union(triangle_subsets[i])

    triangle_subsets = set(frozenset(i) for i in triangle_subsets)

    return triangle_subsets

triangle_subsets = list(create_triangle_subsets(color_set, color_table))

def recurse_rotation(sub, rotations):
    if not sub:
        yield rotations
    else:
        for i in range(3):
            rotations[sub[0]] = i
            yield from recurse_rotation(sub[1:], rotations)


def generate_rotations(subset):
    rotations = { tri: 0 for tri in subset }
    return recurse_rotation(subset, rotations)

def is_solution_no_rotations(subset, color_matrix):
    setA = set()
    setB = set()
    setC = set()

    for tri in subset:
        A = color_matrix[tri-1][0]
        B = color_matrix[tri-1][1]
        C = color_matrix[tri-1][2]

        if A in setA or B in setB or C in setC:
            return False

        setA.add(A)
        setB.add(B)
        setC.add(C)

    return True


def is_solution(subset, rotations, color_matrix):
    setA = set()
    setB = set()
    setC = set()

    for tri in subset:
        rotated_tri = rotate(color_matrix[tri-1], rotations[tri])
        A = rotated_tri[0]
        B = rotated_tri[1]
        C = rotated_tri[2]

        if A in setA or B in setB or C in setC:
            return False

        setA.add(A)
        setB.add(B)
        setC.add(C)

    return True

def process_subsets(subsets, color_matrix):
    min_obs = []
    for sub in subsets:
        if is_solution_no_rotations(sub, color_matrix):
            break

        lst = sorted(list(sub))
        solvable = True
        i = 1
        while solvable and i < 30:
            combs = list(combinations(lst, i))

            for comb in combs:
                has_solution = False
                j = 0
                rotations = generate_rotations(comb)
                while not has_solution:
                    j += 1
                    try:
                        rotation = next(rotations)
                    except StopIteration:
                        solvable = False
                        break
                    if is_solution(comb, rotation, color_matrix):
                        has_solution = True
                if not has_solution:
                    min_obs.append(comb)
                    solvable = False

            i += 1
    if min_obs:
        return min_obs
    else:
        return color_matrix
    return color_matrix

print(process_subsets(triangle_subsets, color_matrix))
