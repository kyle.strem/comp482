import math

color_hist = [0] * 100
color_hist.insert(0, None)
for i in range(1,1000001):
    color = 1 + (math.floor(i * (math.sqrt(13))) % 100)
    color_hist[color] += 1

i = 1
for color in color_hist[1:]:
    print(i, "\t", color)
    i+=1
