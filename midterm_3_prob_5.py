class Matrix:
    def __init__(self, r, c, opts=0):
        self.rows = r
        self.cols = c
        self.opts = opts

    def __repr__(self):
        return "({}x{})".format(self.rows, self.cols)

    def mult(self, other):
        if self.cols == other.rows:
            total = (self.rows * self.cols * other.cols) + other.opts + self.opts
            return Matrix(self.rows, other.cols, total)
        else:
            raise Exception("Only multiply compatible matrices")
